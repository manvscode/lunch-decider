/* Copyright (C) 2012 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package shrewd.applications.lunchdecider;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.content.DialogInterface;
import android.app.Dialog;
import android.app.AlertDialog;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.lang.ClassNotFoundException;

public class ApplicationActivity extends Activity
{
	static final int DIALOG_EXIT = 0;
	static final int DIALOG_ABOUT = 1;
	static final String RESTAURANTS_FILENAME = "lunch-decider-settings.dat";
	protected ArrayList<String> restaurants = null;

	public ApplicationActivity( )
	{
		restaurants = new ArrayList<String>( );
	}

    @Override
    public void onCreate( Bundle state )
	{
        super.onCreate( state );
		//savedInstanceState.putStringArrayList( "restaurants", restaurants );
		if( state != null )
		{
			restaurants = loadArray( RESTAURANTS_FILENAME );
			//restaurants = state.getStringArrayList( "restaurants" );
		}
    }

	@Override
	protected void onSaveInstanceState( Bundle savedInstanceState )
	{
		super.onSaveInstanceState( savedInstanceState );
		//savedInstanceState.putStringArrayList( "restaurants", restaurants );
	}

	@Override
	protected void onRestoreInstanceState( Bundle state )
	{
		if( state != null )
		{
			//restaurants = state.getStringArrayList( "restaurants" );
			restaurants = loadArray( RESTAURANTS_FILENAME );
		}
	}

	protected ArrayList<String> loadArray( String filename )
	{
		ArrayList<String> array = new ArrayList<String>( );

		try
		{
			FileInputStream fis = openFileInput( filename );
			ObjectInputStream ois = new ObjectInputStream( fis );
			array = (ArrayList<String>) ois.readObject( );
		}
		catch( FileNotFoundException e )
		{
			e.printStackTrace();
		}
		catch( IOException e )
		{
			e.printStackTrace( );
		}
		catch( ClassNotFoundException e )
		{
			e.printStackTrace( );
		}

		return array;
	}

	protected void saveArray( String filename, ArrayList<String> array )
	{
		if( array != null )
		{
			FileOutputStream fos = null;

			try
			{
				fos = openFileOutput( filename, MODE_PRIVATE );
				ObjectOutputStream oos = new ObjectOutputStream( fos );
				oos.writeObject( array ); 
				oos.close( );
			}
			catch( FileNotFoundException e )
			{
				e.printStackTrace( );
			}
			catch( IOException e )
			{
				e.printStackTrace( );
			}
		}
	}

	protected Dialog onCreateDialog( int id )
	{
		Dialog dialog;

		switch( id )
		{
			case DIALOG_EXIT:
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setMessage("Are you sure you want to exit?")
					   .setCancelable(false)
					   .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						   public void onClick(DialogInterface dialog, int id) {
								ApplicationActivity.this.finish();
						   }
					   })
					   .setNegativeButton("No", new DialogInterface.OnClickListener() {
						   public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
						   }
					   });
				dialog = builder.create();
				break;
			}
			case DIALOG_ABOUT:
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setMessage("I hope you enjoyed this app as much as I enjoyed making it!\n\nCopyright 2012, Joe Marrero.\nhttp://www.manvscode.com/")
					   .setCancelable(true);
					   //.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						   //public void onClick(DialogInterface dialog, int id) {
								//LunchDeciderActivity.this.finish();
						   //}
					   //});
				dialog = builder.create();
				break;
			}
			default:
			{
				dialog = null;
				break;
			}
		}

		return dialog;
	}

}
