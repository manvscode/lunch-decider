/* Copyright (C) 2012 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package shrewd.applications.lunchdecider;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;

public class SettingsActivity extends ApplicationActivity
{
    @Override
    public void onCreate( Bundle savedInstanceState )
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
		loadControls();
    }

	protected void loadControls( )
	{
		EditText[] restaurantSettings = {
			(EditText) findViewById( R.id.setting_restaurant0 ),
			(EditText) findViewById( R.id.setting_restaurant1 ),
			(EditText) findViewById( R.id.setting_restaurant2 ),
			(EditText) findViewById( R.id.setting_restaurant3 ),
			(EditText) findViewById( R.id.setting_restaurant4 ),
			(EditText) findViewById( R.id.setting_restaurant5 ),
			(EditText) findViewById( R.id.setting_restaurant6 ),
			(EditText) findViewById( R.id.setting_restaurant7 )
		};

		restaurants = super.loadArray( super.RESTAURANTS_FILENAME );
		int index = 0;

		for( EditText et : restaurantSettings )
		{
			if( index >= restaurants.size() )
			{
				break;
			}

			if( et != null )
			{
				String s = restaurants.get( index );
				et.setText( s );
				index++;	
			}
		}
	}

	protected void saveControls( )
	{
		EditText[] restaurantSettings = {
			(EditText) findViewById( R.id.setting_restaurant0 ),
			(EditText) findViewById( R.id.setting_restaurant1 ),
			(EditText) findViewById( R.id.setting_restaurant2 ),
			(EditText) findViewById( R.id.setting_restaurant3 ),
			(EditText) findViewById( R.id.setting_restaurant4 ),
			(EditText) findViewById( R.id.setting_restaurant5 ),
			(EditText) findViewById( R.id.setting_restaurant6 ),
			(EditText) findViewById( R.id.setting_restaurant7 )
		};

		super.restaurants.clear();
		for( EditText et : restaurantSettings )
		{
			String s = et.getText().toString().trim();

			if( s.length() > 0 )
			{
				super.restaurants.add( s );
			}
		}
	
		super.saveArray( super.RESTAURANTS_FILENAME, restaurants );
	}

	public void onSave( View v )
	{
		saveControls();
		finish();
	}

	public void onCancel( View v )
	{
		finish();
	}
}
