/* Copyright (C) 2012 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package shrewd.applications.lunchdecider;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.DialogInterface;
import android.app.Dialog;
import android.app.AlertDialog;
import android.widget.TextView;
import java.util.Random;

public class LunchDeciderActivity extends ApplicationActivity
{
    @Override
    public void onCreate( Bundle savedInstanceState )
	{
        super.onCreate( savedInstanceState );
        setContentView( R.layout.lunch_decider );
		restaurants = super.loadArray( super.RESTAURANTS_FILENAME );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
	{
        getMenuInflater().inflate(R.menu.lunch_decider, menu);
        return true;
    }

	public void onWhatsForLunch( View v )
	{
		if( restaurants.size() > 0 )
		{
			Random rand = new Random();
			int r = rand.nextInt( restaurants.size() );

			if( r < restaurants.size() )
			{
				String s = restaurants.get( r );
				TextView et = (TextView) findViewById( R.id.chosen_restaurant );
				et.setText( s );
			}
		}
	}
	
	public void onSettings( View v )
	{
		Intent i = new Intent( this, SettingsActivity.class );
		startActivity( i );
		restaurants = super.loadArray( super.RESTAURANTS_FILENAME );
	}

	public void onAbout( View v )
	{
		Intent i = new Intent( this, AboutActivity.class );
		startActivity( i );
		//this.showDialog( DIALOG_ABOUT );
	}

	public void onExit( View v )
	{
		this.showDialog( DIALOG_EXIT );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item )
	{
		boolean result = false;

		switch( item.getItemId() )
		{
			case R.id.menu_exit:
				onExit( null );
				result = true;
				break;
			case R.id.menu_about:
				onAbout( null );
				result = true;
				break;
			case R.id.menu_settings:
				onSettings( null );
				result = true;
				break;
			default:
				result = super.onOptionsItemSelected(item);
				break;
		}

		return result;
	}
}
